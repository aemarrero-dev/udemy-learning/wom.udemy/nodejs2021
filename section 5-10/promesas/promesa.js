function getCar(id) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            console.log('Coche obtenido 23 desde la DB');
            resolve({id: 23, marca: 'Masserati', model: 'SUX2'})
        }, 3000);
    });
}

function getModel(model) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            console.log('Modelo obtenido: SUX2 desde la DB');
            resolve({speed: 300, seat: 2, motor: 'V8'});
        }, 3000);
    })
}

/*
const promesa = getCar(23);
//promesa.then(car => console.log(car))
promesa
    .then(car => getModel(car.model))
    .then(model => console.log(model))
    .catch(err => console.log(err.message))

*/

// USO DE ASYNC Y AWAIT. DEBE IR DENTRO DE TRY-CATCH
async function showModel() {
    try {
        const car = await getCar(23);
        const model = await getModel(car.model);
        console.log('Model: ', model);
    } catch(err) {
        console.log(err.message);
    }
}

showModel();