//EJEMPLO DE LLAMAR PROMESAS EN PARALELO
const promesa1 = new Promise((resolve, reject) => {
    setTimeout(() => {
        console.log('Leyendo datos desde Facebook');
        resolve({amigos: 50, likes: 250});
    }, 1000);
});

const promesa2 = new Promise((resolve, reject) => {
    setTimeout(() => {
        console.log('Leyendo datos desde Twitter');
        resolve({amigos: 200, tweets: 500});
    }, 4000);
});

/*
Promise.all([promesa1, promesa2])
    .then(result => console.log(result))
    .catch(err => console.log(err.message))
*/

//ENVIA LA PRIMERA PROMISE
Promise.race([promesa1, promesa2])
    .then(result => console.log(result))
    .catch(err => console.log(err.message))