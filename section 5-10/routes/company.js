const mongoose = require('mongoose'); //agregamos mongoose para modelar entidades
const express = require('express');
const {check, validationResult} = require('express-validator'); //Ver documentacion
const router = express.Router();
//const Company = require('../models/company'); 
const {Company} = require('../models/company'); //importamos asi para modelo embebido

router.get('/', async (req, res) => {
    const companies = await Company.find();
    res.send(companies);
    console.log(companies);
});

router.get('/:id', async (req, res) => {
    const company = await Company.findById(req.params.id);
    if(!company) return res.status(404).send('Compañia no encontrada');
    res.send(company)
});

router.post('/', [
    check('name').isLength({min: 3}),
    check('country').isLength({min: 3})
], async (req, res) => {
    
    const errors = validationResult(req);
    if(!errors.isEmpty()) {
        res.status(422).send({errors: errors.array()});
    } else {
        var company = new Company({
            name: req.body.name,
            country: req.body.country
        });
    
        const result = await company.save();
        res.status(201).send(result);
    }
});

//Usando PUT con express-validator
router.put('/:id', [
    check('name').isLength({min: 3}),
    check('country').isLength({min: 3})
], async (req, res) => {
    
    const errors = validationResult(req);
    if(!errors.isEmpty()) {
        res.status(422).send({errors: errors.array()});
    } else {
        //ver mongoose api docs
        const company = await Company.findByIdAndUpdate(req.params.id, {
            name: req.body.name,
            country: req.body.country
        }, {new: true});
        
        if(!company) {
            res.status(404).send('Compañia con ID: ' + req.params.id + ' no existe.');
        }
        res.status(204).send();
    }
});

//Usando DELETE
router.delete('/:id', async (req, res) => {
    
    //Metodo find()
    const company = await Company.findByIdAndDelete(req.params.id);

    if(!company) {
        res.status(404).send('Compañia con ID: ' + req.params.id + ' no existe y no pudo ser borrado.');
    }
    
    res.status(200).send('Compañia borrada con exito.');
});

module.exports = router