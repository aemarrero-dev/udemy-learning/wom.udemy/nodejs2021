//routes o como nuestro controller car

const mongoose = require('mongoose'); //agregamos mongoose para modelar entidades
const express = require('express');
const {check, validationResult} = require('express-validator'); //Ver documentacion
//const app = express();
const router = express.Router();

const Car = require('../models/car'); //importamos model Car
const {Company} = require('../models/company'); //importamos company para modelo embebido
const authorize = require('../middleware/role');
const auth = require('../middleware/authjwt');
const Role = require('../helpers/role');

/* comentamos porque ahora usaremos datos de mongo
var cars = [
    { id:0, marca: 'Ford', model:'Ka', year: 2020 },
    { id:1, marca: 'Porsche', model:'Caiman', year: 2019 },
    { id:2, marca: 'Mitsubishi', model:'Lancer', year: 2020 },
    { id:3, marca: 'Subaru', model:'GT', year: 2021 },
    { id:4, marca: 'Toyota', model:'Camry', year: 2018 }
];*/

/******************  ENDPOINTS ********************/

/*router.get('/list', (req, res) => {
    res.send(['Audi', 'Mercedez Benz', 'Porsche', 'Volkswagen', "Chevrolet", 'Fiat']);
});*/

router.get('/', [auth, authorize([Role.Admin, Role.User])], async (req, res) => {
    //const cars = await Car.find(); para modelo de documentos
    
    //para modelo normalizado, nos trae name, country de company en vez de solo id
    //const cars = await Car.find().populate('company', 'name country'); modelo normalizado
    const cars = await Car.find(); 
    res.send(cars);
    console.log(cars);
});

//PathVariable id
router.get('/:id', async (req, res) => {
    const car = await Car.findById(req.params.id);
    if(!car) return res.status(404).send('Car no encontrado');
    res.send(car)
});

//Modelo embebido
router.post('/', [
    check('model').isLength({min: 3}),
    check('year').isNumeric()
], async (req, res) => {
    
    const errors = validationResult(req);
    if(!errors.isEmpty()) {
        res.status(422).send({errors: errors.array()});
    } else {

        const company = await Company.findById(req.body.companyId);
        if(!company) {
            res.status(400).send('Compañia no encontrada');
        }

        var car = new Car({
            company: company,
            model: req.body.model,
            year: req.body.year,
            sold: req.body.sold,
            price: req.body.price,
            extras: req.body.extras
        });
    
        const result = await car.save();
        res.status(201).send(result);
    }
});

//Modelo normalizado
/*router.post('/', [
    //check('company').isLength({min: 3}), comentamos para probar relacion car-company
    check('model').isLength({min: 3}),
    check('year').isNumeric()
], async (req, res) => {
    
    const errors = validationResult(req);
    if(!errors.isEmpty()) {
        res.status(422).send({errors: errors.array()});
    } else {
        var car = new Car({
            company: req.body.company,
            model: req.body.model,
            year: req.body.year,
            sold: req.body.sold,
            price: req.body.price,
            extras: req.body.extras
        });
    
        const result = await car.save();
        res.status(201).send(result);
        //console.log(car);
    }
});*/

//Usando PUT con express-validator
router.put('/:id', [
    check('company').isLength({min: 3}),
    check('model').isLength({min: 3}),
    check('year').isNumeric()
], async (req, res) => {
    
    const errors = validationResult(req);
    if(!errors.isEmpty()) {
        res.status(422).send({errors: errors.array()});
    } else {
        //ver mongoose api docs
        const car = await Car.findByIdAndUpdate(req.params.id, {
            company: req.body.company,
            model: req.body.model,
            year: req.body.year,
            sold: req.body.sold,
            price: req.body.price,
            extras: req.body.extras
        }, {new: true});
        
        if(!car) {
            res.status(404).send('Ese carro con ID: ' + req.params.id + ' no existe.');
        }
        res.status(204).send();
    }
});

//Usando DELETE
router.delete('/:id', async (req, res) => {
    
    //Metodo find()
    const car = await Car.findByIdAndDelete(req.params.id);

    if(!car) {
        res.status(404).send('El carro con ID: ' + req.params.id + ' no existe y no pudo ser borrado.');
    }
    
    res.status(200).send('Carro borrado con exito.');
});

/*
//PathVariable id
router.get('/:company/:model', (req, res) => {
    //res.send(req.params.company);
    //res.send(req.params.model);
    res.send(req.params);
});



router.get('/:marca', (req, res) => {
    const car = cars.find(c => c.marca === req.params.marca);
    if(!car) {
        res.status(404).send("No existe esa marca");  
    } else {
        res.send(car);
    }
    
});

//Usando POST con body request
router.post('/', (req, res) => {
    var carId = cars.length;
    var car = {
        id: carId,
        marca: req.body.marca || '',
        model: req.body.model || '',
        year: req.body.year || ''
    }
    console.log(car);
    cars.push(car);
    res.status(201).send(car);
});

//Usando POST con validacion de body request
router.post('/v2', (req, res) => {
    //Validamos request
    if(!req.body.marca || req.body.marca.length < 3) {
        res.status(400).send('Valor de marca incorrecto');
    }
    if(!req.body.model || req.body.model.length < 3) {
        res.status(400).send('Valor de model incorrecto');
    }

    var carId = cars.length;
    var car = {
        id: carId,
        marca: req.body.marca || '',
        model: req.body.model || '',
        year: req.body.year || ''
    }
    console.log(car);
    cars.push(car);
    res.status(201).send(car);
});

//Usando POST con express-validator
router.post('/v3', [
    check('marca').isString().isLength({min: 5}),
    check('model').isLength({min: 2}),
    check('year').isNumeric()
], (req, res) => {
    
    const errors = validationResult(req);
    if(!errors.isEmpty()) {
        res.status(422).send({errors: errors.array()});
    }
    var carId = cars.length;
    var car = {
        id: carId,
        marca: req.body.marca || '',
        model: req.body.model || '',
        year: req.body.year || ''
    }
    console.log(car);
    cars.push(car);
    res.status(201).send(car);
});

//Usando PUT con express-validator
router.put('/:id', [
    check('marca').isString().isLength({min: 3}),
    check('model').isLength({min: 2}),
    check('year').isNumeric()
], (req, res) => {
    
    const errors = validationResult(req);
    if(!errors.isEmpty()) {
        res.status(422).send({errors: errors.array()});
    }
    //Metodo find()
    const car = cars.find(car => car.id === parseInt(req.params.id));

    if(!car) {
        res.status(404).send('Ese carro con ID: ' + req.params.id + ' no existe.');
    }
    
    car.marca = req.body.marca;
    car.model = req.body.model;
    car.year = req.body.year;

    res.status(204).send();
});

//Usando DELETE
router.delete('/:id', (req, res) => {
    
    //Metodo find()
    const car = cars.find(car => car.id === parseInt(req.params.id));

    if(!car) {
        res.status(404).send('Ese carro con ID: ' + req.params.id + ' no existe.');
    }
    
    const index = cars.indexOf(req.params.id);
    cars.splice(index, 1);
    res.status(200).send('Carro borrado con exito.');
});
*/
module.exports = router