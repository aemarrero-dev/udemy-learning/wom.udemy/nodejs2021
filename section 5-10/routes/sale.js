const mongoose = require('mongoose');
const express = require('express');
const {check, validationResult} = require('express-validator'); //Ver documentacion
const router = express.Router();
const Sale = require('../models/sale'); 
const Car = require('../models/car');
const User = require('../models/user');
const auth = require('../middleware/authjwt');
const adminAccess = require('../middleware/admin');

//AHORA ESTE ENDPOINT LO EJECUTARA UN USUARIO ADMIN. SE VALIDA TOKEN Y LUEGO ACCESO ADMIN
router.get('/', [auth, adminAccess], async (req, res) => {
    const sales = await Sale.find();
    res.send(sales);
    console.log(sales);
});

//AGREGAMOS AUTH PARA QUE SOLO USUARIOS VALIDOS PUEDAN EJECUTAR UNA COMPRA
router.post('/', auth, 
[check('price').isNumeric()], async (req, res) => {
    
    const errors = validationResult(req);
    if(!errors.isEmpty()) {
        res.status(422).send({errors: errors.array()});
    } else {
        const user = await User.findById(req.body.userId);
        if(!user) {
            res.status(400).send('Usuario no encontrado');
        }

        const car = await Car.findById(req.body.carId);
        if(!car) {
            res.status(400).send('Carro no encontrado');
        }

        if(car.sold === true) {
            res.status(400).send('Carro ya fue vendido anteriormente');
        }

        const sale = new Sale({
            user: {
                _id: user._id,
                name: user.name,
                email: user.email
            },
            car: {
                _id: car._id,
                model: car.model
            },
            price: req.body.price
        });
    
        /*
        const result = await sale.save();
        res.status(201).send(result);
        user.isCustomer = true;
        user.save();
        car.sold = true;
        car.save();
        res.status(201).send(result);
        */

       //MEJORAMOS ESTAS OPERACIONES USANDO TRANSACCIONES
       const session = await mongoose.startSession();
       session.startTransaction();
       try {
           const result = await sale.save();
           res.status(201).send(result);
           user.isCustomer = true;
           user.save();
           car.sold = true;
           car.save();
           await session.commitTransaction();
           session.endSession();
           res.status(201).send(result);
       } catch(e) {
           await session.abortTransaction();
           session.endSession();
           res.status(500).send(e.message);
       }
    }
});

module.exports = router