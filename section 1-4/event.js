//const EventEmitter = require("events");
import {EventEmitter} from 'events';

const eventEmitter = new EventEmitter();

eventEmitter.on("event_example", function() {
    console.log("Un evento ha ocurrido");
});

eventEmitter.emit("event_example");


eventEmitter.on("event_arg", function(arg) {
    console.log("Un evento ha ocurrido con id: " + arg.id + ", message: " + arg.message);
});

eventEmitter.emit("event_arg", {id: 3005, message: "500 Internal error"});

//MISMNO EVENTO PERO CON ARROW FUNCTIONS
eventEmitter.on("event_arrow", (arg) => {
    console.log("-> Un evento ha ocurrido con id: " + arg.id + ", message: " + arg.message);
});

eventEmitter.emit("event_arrow", {id: 212011, message: "Customer B2b"});
