//const http = require("http");
import * as http from 'http';

/*
const server = http.createServer();

server.on("connection", (socket) => {
    console.log("Nueva conexion detectada");
});

server.listen(2012);
console.log("Listening on port 2012....");
*/

/*
const server = http.createServer((req, res) => {
    if(req.url === "/") {
        res.write("<h1>Hola Mundo desde Servidor</h1>");
        res.end();
    }

    if(req.url === "/coches") {
        res.write("<h1>Listado de coches</h1>");
        res.end();
    }
});

server.listen(3030);
console.log("Listening on port 3030.....");
*/

const server = http.createServer((req, res) => {
    res.writeHead(200, {"Content-Type": "text/html"});
    res.write("<h1>Hola Mundo desde Servidor</h1>");
    res.write("<p>Mi lista de coches</p>");
    res.end();
}).listen(5050);

