// 1. Siempre crear proyecto con npm init
// 2. Instalamos express con npm install express
// 3. Instalamos nodemon para que actualice nuestros cambios sin reiniciar servidor.
// 4. Ahora ejecutamos los archivos con nodemon XXX.js
// 5. Ejecutamos comando Get-ExecutionPolicy y Set-ExecutionPolicy Unrestricted

const mongoose = require('mongoose'); //agregamos mongooose para modelar y conectarnos a mongo
const express = require('express');
const car = require('./routes/car');
const user = require('./routes/user');
const company = require('./routes/company');
const sale = require('./routes/sale');
const auth = require('./routes/auth');
const morgan = require('morgan'); //MORGAN ES UN HTTP-REQUEST LOGGER. npm install morgan

//const bodyParser = require('body-parser');
const app = express();
const port = process.env.PORT || 3003

//const date = require('./date');

app.use(morgan('tiny'));

//Esto es para parsear request a JSON cuando usamos post
app.use(express.json());
app.use(express.urlencoded({extended:true}));

//USAMOS MODULO CAR Y TIENE URL BASE /api/cars

app.use('/api/cars/', car);
app.use('/api/users/', user);
app.use('/api/companies/', company);
app.use('/api/sales/', sale);
app.use('/api/auth/', auth);


/* Deprecated
app.use(bodyParser.urlencoded());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));
*/

//PROBANDO EL USO DE FUNCION MIDDLEWARE
/*app.use(function(req, res, next) {
    //console.log('Time: ', Date.now());
    console.log('Request-Type: ', req.method);
    next(); //Si omitimos el next(), se queda en estado loading
});*/

//MIGRAMOS LA FUNCION A DATE.JS Y LA IMPORTAMOS AQUI
//app.use(date);

app.listen(port, () => console.log('Server listening at port ' + port));

console.log('secret: ', process.env.SECRET_KEY_JWT_API);

const mongoURI = 'mongodb://andres:nodejs2021@45.79.84.26:27017/dbudemy?authSource=admin';
//CONEXION A MONGO
mongoose.connect(mongoURI, {
    useNewUrlParser: true,
    useFindAndModify: false,
    useUnifiedTopology: true,
    useCreateIndex: true
    })
    .then(() => console.log('Conectado correctamente a MongoDB'))
    .catch(() => console.log('Error conectando a MongoDB'));
