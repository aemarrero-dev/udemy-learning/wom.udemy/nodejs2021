//PROBANDO EL USO DE FUNCION MIDDLEWARE
function date(req, res, next) {
    console.log('Time: ', Date.now());
    next(); //Si omitimos el next(), se queda en estado loading
};

module.exports= date;