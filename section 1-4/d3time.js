//import {timeDay} from "d3-time"; //funcion individual
import * as d3time from 'd3-time'; //todas las funciones

var start = new Date(2021, 1, 1);

var end = new Date(2021, 12, 31);

var miliSegundosDia = 24*60*60*1000;

var resultado = (end-start)/miliSegundosDia;

console.log("old: " + resultado);

resultado = d3time.timeDay.count(start, end);

console.log("d3-time: " + resultado);