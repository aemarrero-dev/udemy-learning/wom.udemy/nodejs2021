const promesa = new Promise((resolve, reject) => {
    setTimeout(() => {
        resolve({id: 1, model: 'Massetari', marca: 'XSD'})
        //reject(new Error('Se ha producido un error al leer la BD'))
    }, 4000);
});

promesa
    .then(result => console.log(result))
    .catch(err => console.log(err))
    