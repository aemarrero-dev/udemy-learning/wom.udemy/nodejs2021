const mongoose = require('mongoose'); //agregamos mongoose para modelar entidades
const jwt = require('jsonwebtoken');

//CREAMOS SCHEMA y MODELO USER
const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        uppercase: true,
        trim: true,
        minlength: 3,
        maxlength: 99
    },
    isCustomer: Boolean,
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    //isAdmin: Boolean, //agregamos campo para que solo admin ejecute GET /sales
    role: String, //ahora tenemos los roles en un helper. (Admin, Editor, User)
    date: {type: Date, default: Date.now}
});

//REFACTORIZAMOS Y CREAMOS TOKEN DENTRO DE FUNCION. CREAMOS DATOS DEL PAYLOAD
userSchema.methods.generateJWT = function () {
    const payload = {
        _id: this._id, 
        name: this.name,
        //isAdmin: this.isAdmin //agregamos isAdmin en payload del jwt
        role: this.role //ahora manejamos los roles asi

    };
    //return jwt.sign(payload, 'secret'); //sign({payload}, secret)
    return jwt.sign(payload, process.env.SECRET_KEY_JWT_API); //GUARDAMOS SECRET COMO VARIABLE DE ENTORNO
}

const User = mongoose.model('user', userSchema);

module.exports = User;