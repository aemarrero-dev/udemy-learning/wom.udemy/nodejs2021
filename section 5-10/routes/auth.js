//INSTALAMOS npm install jsonwebtoken

const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const mongoose = require('mongoose');
const express = require('express');
const {check, validationResult} = require('express-validator');
const router = express.Router();
const User = require('../models/user');

router.post('/', [
    check('email').isEmail().isEmail(),
    check('password').isLength({min: 3})
], async (req, res) => {
    
    const errors = validationResult(req);
    if(!errors.isEmpty()) {
        res.status(422).send({errors: errors.array()});
    } else {
        let user = await User.findOne({email: req.body.email});
        if(!user) { return res.status(400).send('Usuario o contraseña incorrectos'); }

        //COMPARAMOS CLAVE ENCRIPTADA CON LA CLAVE INGRESADA.
        const validPassword = await bcrypt.compare(req.body.password, user.password);
        if(!validPassword) { return res.status(400).send('Usuario o contraseña incorrectos'); }

        //sign({payload}, secret)
        //const jwtToken = jwt.sign({_id: user._id, name: user.name}, 'secret');
        const jwtToken = user.generateJWT();
        
        res.status(201)
            .header('Authorization', jwtToken)
            .send({
                _id: user._id,
                name: user.name,
                email: user.email
            });
        //res.send('Usuario y contraseña correcta');
        //res.send(jwtToken);
    }
});

module.exports = router;