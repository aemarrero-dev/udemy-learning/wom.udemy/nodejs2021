//1. Instalamos npm install bcrypt para encriptar contraseñas

//const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const mongoose = require('mongoose'); //agregamos mongoose para modelar entidades
const express = require('express');
const {check, validationResult} = require('express-validator'); //Ver documentacion
const router = express.Router();
const User = require('../models/user'); 


router.get('/', async (req, res) => {
    const users = await User.find();
    res.send(users);
    console.log(users);
});

//PathVariable id
router.get('/:id', async (req, res) => {
    const user = await User.findById(req.params.id);
    if(!user) return res.status(404).send('User no encontrado');
    res.send(user)
});

//Usando POST con express-validator
router.post('/', [
    check('name').isLength({min: 3}),
    check('email').isEmail().isEmail(),
    check('isCustomer').isBoolean(),
    check('password').isLength({min: 3})
], async (req, res) => {
    
    const errors = validationResult(req);
    if(!errors.isEmpty()) {
        res.status(422).send({errors: errors.array()});
    } else {
        
        let user = await User.findOne({email: req.body.email});
        if(user) {
            return res.status(400).send('Ese usuario ya existe');
        }

        //ENCRIPTADO DE PASSWORD CON BCRYPT
        const salt = await bcrypt.genSalt(10);
        const hashPassword = await bcrypt.hash(req.body.password, salt);

        user = new User({
            name: req.body.name,
            email: req.body.email,
            password: hashPassword,
            isCustomer: false,
            //isAdmin: req.body.isAdmin,
            role: req.body.role
        });
    
        const result = await user.save();
        
        /*const payload = {_id: user._id, name: user.name};
        const jwtToken = jwt.sign(payload, 'secret'); //sign({payload}, secret)
        */
        const jwtToken = user.generateJWT();
        res.status(201)
            .header('Authorization', jwtToken)
            .send({
                _id: user._id,
                name: user.name,
                email: user.email
            });
    }
});

//Usando PUT con express-validator
router.put('/:id', [
    check('name').isLength({min: 3}),
    check('email').isEmail().isEmail(),
    check('isCustomer').isBoolean()
], async (req, res) => {
    
    const errors = validationResult(req);
    if(!errors.isEmpty()) {
        res.status(422).send({errors: errors.array()});
    } else {
        //ver mongoose api docs
        const user = await User.findByIdAndUpdate(req.params.id, {
            name: req.body.name,
            email: req.body.email,
            isCustomer: req.body.isCustomer
        }, {new: true});
        
        if(!user) {
            res.status(404).send('Ese usuario con ID: ' + req.params.id + ' no existe.');
        }
        res.status(204).send();
    }
});

//Usando DELETE
router.delete('/:id', async (req, res) => {
    
    //Metodo find()
    const user = await User.findByIdAndDelete(req.params.id);

    if(!user) {
        res.status(404).send('El usuario con ID: ' + req.params.id + ' no existe y no pudo ser borrado.');
    }
    
    res.status(200).send('Usuario borrado con exito.');
});

module.exports = router