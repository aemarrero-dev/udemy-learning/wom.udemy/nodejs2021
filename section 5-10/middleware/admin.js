module.exports = function(req, res, next) {
    if(!req.user.isAdmin) {
        return res.status(403).send('Acceso denegado. Usuario no admin');
    }
    next(); //siguiente middleware
}

//NO SE USA AHORA, ESTAMOS MANEJANDO DISTINTO LOS ROLES