import * as d3time from 'd3-time'

var start = new Date(1984, 1, 13);

var end = new Date(2021, 6, 17);

var miliSegundosDia = 24*60*60*1000;

var resultado = (end-start)/miliSegundosDia;

console.log("old: " + resultado);

resultado = d3time.timeDay.count(start, end);

console.log("d3-time - edad: " + resultado / 365);