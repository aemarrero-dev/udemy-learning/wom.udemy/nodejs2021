import { release, freemem, totalmem } from 'os'; //ES6 Module

console.log("Version OS", release());

console.log("Memory Free", freemem());

console.log("Total Memory " + totalmem());