const mongoose = require('mongoose'); //agregamos mongoose para modelar entidades
const {company, companySchema} = require('./company');

//CREAMOS SCHEMA y MODELO CAR
const carSchema = new mongoose.Schema({
    
    //Modelo embebido
    company: {
        type: companySchema,
        required: true
    },

    //Modelo normalizado
    /*company: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'company'
    },*/

    //Tablas sin relacion
    /*company: {
        type: String,
        required: true,
        uppercase: true,
        trim: true,
        minlength: 2,
        maxlength: 99
        //enum: ['BMW', 'AUDI', 'SEAT']
    },*/
    model: String,
    sold: Boolean,
    price: {
        type: Number,
        required: function() { //required tendra el valor de sold
            return this.sold
        }
    },
    year: {
        type: Number,
        min: 2000,
        max: 2030,
        get: y => Math.round(y) //validacion en caso que año venga como 2020.5 se redondea a 2020
    },
    extras: [String],
    date: {type: Date, default: Date.now},
});

const Car = mongoose.model('car', carSchema);

module.exports = Car;