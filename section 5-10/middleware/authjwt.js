//MIDDLEWARE AUTH. Valida que exista el token

const jwt = require('jsonwebtoken');

function auth(req, res, next) {
    //OBTENEMOS TOKEN DESDE EL HEADER
    const jwtToken = req.header('Authorization');
    if(!jwtToken) return res.status(401).send('Acceso denegado. Tokem inexistente');

    try {
        //VERIFICAMOS TOKEN CON EL SECRET
        //const payload = jwt.verify(jwtToken, 'password');
        const payload = jwt.verify(jwtToken, process.env.SECRET_KEY_JWT_API);
        req.user = payload;
        next();
    } catch(e) {
        res.status(400).send('Acceso denegado. Token invalido');
    }
}

module.exports = auth;