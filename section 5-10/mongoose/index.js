/*
    1. LA IDEA ERA CREAR UN PROYECTO NUEVO LLAMADO MONGOOSE
    2. EJECUTAR NPM INIT
    SE DECIDIO SOLO AGREGAR UNA CARPETA MAS LLAMADA MONGOOSE Y UTILIZAR PROYECTO SECTION 5-10
    EJECUTAMOS CON node index.js
*/

const mongoose = require('mongoose');
mongoose.connect('mongodb://45.79.84.26/dbtest', {
    useNewUrlParser: true, 
    useFindAndModify: false
    })
    .then(() => console.log('Conectado correctamente a MongoDB'))
    .catch(() => console.log('Error conectando a MongoDB'));


const carSchema = new mongoose.Schema({
    company: String,
    model: String,
    price: Number,
    year: Number,
    sold: Boolean,
    extras: [String],
    date: {type: Date, default: Date.now}
})

const Car = mongoose.model('car', carSchema);

/* OPERADORES
    $eq = igual
    $ne = no igual
    $gt = mayor que
    $gte = mayor o igual que
    $lt = menor que
    $lte = menor o igual que
    $in = contiene (uso en arrays)
    $nin = no contiene (uso en arrays)
    $and = operacion AND
    $or = operacion OR
*/

//LLAMADA METODO DELETE
//deleteCar('60da0432ae2e4942802375aa');
async function deleteCar(id) {
    const filter = {_id: id};
    const result = await Car.deleteOne(filter)
    console.log(result);
}

//LLAMADA METODO UPDATE.
//updateFirstCar('60da0432ae2e4942802375aa')
async function updateFirstCar(id) {
    const filter = {_id: id};
    const update = {model: 'Spark GT', year: 2020, sold: false}
    const car = await Car.findOneAndUpdate(filter, update);
    /*const car = await Car.update(
        {_id: id}, 
        {
            $set: {
                model: 'Camaro GT',
                year: 2020,
                sold: false
            }
        } 
    );*/
    console.log(car);
}

//LLAMADA METODO UPDATE CON DOS OPERACIONES.
//updateCar('60da0432ae2e4942802375aa')
async function updateCar(id) {
    const car = await Car.findById(id);
    if(!car) return;
    //modificamos y guardamos
    car.sold = true;
    car.year = 2014;
    car.model = 'Spark GT';
    const result = await car.save();
    console.log(result);
}

//LLAMADA METODO CON PAGINACION.
//getPaginationCars()
async function getPaginationCars() {
    const pageNumber = 2; //indica que pagina devolver
    const pageSize = 2;
    const cars = await Car
        .find()
        .skip((pageNumber-1)*pageSize) //salta paginas
        .limit(pageSize);
    console.log(cars);
}

//LLAMADA METODO COUNT. SE PUEDE USAR COUNT() SOLO, COUNTDOCUMENTS() O CONCATENAR CON FIND()
//getCountCar()
async function getCountCar() {
    const cars = await Car
        .find({company: 'Ford'})
        //.count()
        .countDocuments()
    console.log(cars);
}

//LLAMADA METODO CON FILTROS AND/OR
//getFilterPriceAndOrCars()
async function getFilterPriceAndOrCars() {
    const cars = await Car
        .find()
        .or([{company: 'Ford'}, {model: 'Mustang GT'}])
        //.and([{company: 'Ford'}, {model: 'Mustang GT'}])
    console.log(cars);
}

//LLAMADA METODO CON IN Y NIN
//getFilterPriceInNinCars()
async function getFilterPriceInNinCars() {
    const cars = await Car
        //.find({extras: {$in: 'Automatic'}})
        .find({extras: {$nin: 'Automatic'}})
    console.log(cars);
}


//LLAMADA DE METODO CON MAS FILTROS, LIMIT, SORT Y SELECT
//getFilterPriceCars();
async function getFilterPriceCars() {
    const cars = await Car
        .find({price: {$gte: 500000, $lt: 950000}})
        //.find({price: {$lt: 700000}})
    console.log(cars);
}

//LLAMADA DE METODO CON MAS FILTROS, LIMIT, SORT Y SELECT
//getMoreFilterCar()
async function getMoreFilterCar() {
    const cars = await Car
        .find({company: 'Ford', sold: true})
        .sort({price: -1}) //-1 de mayor a menor. 1 de menor a mayor
        .limit(3)
        .select({company: 1, model: 1, price: 1}); //mostrtamos solo esos 3 campos. 1 para mostrar

    console.log(cars);
}

//LLAMADA DE METODO CON MAS FILTROS DE BUSQUEDA
//getCompanyAndSoldFilterCars()
async function getCompanyAndSoldFilterCars() {
    const cars = await Car.find({company: 'Ford', sold: true});
    console.log(cars);
}

//LLAMADA A METODO FIND ALL CARS
//getCars();
async function getCars() {
    const cars = await Car.find();
    console.log(cars);
}

//FUNCION ASYNC QUE GUARDA REGISTRO EN DB
createCar();
async function createCar(){
    const car = new Car({
        company: 'General Motors',
        model: 'Aveo',
        price: 150000,
        year: 2015,
        sold: false,
        extras: ['Manual', 'Air Conditioning']
    });

    const result = await car.save();
    console.log(result);
}