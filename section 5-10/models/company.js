const mongoose = require('mongoose'); //agregamos mongoose para modelar entidades

//CREAMOS SCHEMA y MODELO USER
const companySchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        uppercase: true,
        trim: true,
        minlength: 3,
        maxlength: 20
    },
    country: String,
    date: {type: Date, default: Date.now},
});

const Company = mongoose.model('company', companySchema);

//module.exports = Company;
module.exports.Company = Company;
module.exports.companySchema = companySchema; //exportamos el schema para uso de modelo embebido