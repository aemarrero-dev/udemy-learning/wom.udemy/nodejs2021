/**
 * VALIDACION DE SCHEMAS CON MONGOOSE
 * 
*/

//CONEXION DE LA BD
const mongoose = require('mongoose');
mongoose.connect('mongodb://45.79.84.26/dbtest2', {
    useNewUrlParser: true, 
    useFindAndModify: false
    })
    .then(() => console.log('Conectado correctamente a MongoDB'))
    .catch(() => console.log('Error conectando a MongoDB'));

//SCHEMAS
const carSchema = new mongoose.Schema({
    company: {
        type: String,
        required: true,
        lowercase: true,
        //uppercase: true,
        trim: true,
        minlength: 3,
        maxlength: 20,
        //enum: ['BMW' , 'AUDI'] //en caso de limitar los valores
    },
    model: String,
    sold: Boolean,
    price: {
        type: Number,
        required: function() { //required tendra el valor de sold
            return this.sold
        }
    },
    year: {
        type: Number,
        min: 2000,
        max: 2030,
        get: y => Math.round(y) //validacion en caso que año venga como 2020.5 se redondea a 2020
    },
    extras: [String],
    date: {type: Date, default: Date.now},
});

const Car = mongoose.model('car', carSchema);

createCar();
async function createCar() {
    const car = new Car({
        company: 'BMW',
        model: 'X7',
        price: 6000,
        year: 2024,
        sold: true,
        extras:['4*4']
    });
    try {
        const result = await car.save();
        console.log(result);
    } catch(e) {
        console.group(e); 
    }

}

